IMAGE=service-observer-plugin
BINDIR=$(shell pwd)/bin

build:
	docker build -t ${IMAGE}:base -f Dockerfile_base .
	make buildBin

buildBin:
	docker build -t ${IMAGE}:build -f Dockerfile_build .
	docker run --rm -v ${BINDIR}:/app/bin ${IMAGE}:build

rebuild: clean
	make buildBin

clean:
	docker rmi ${IMAGE}:build

cleanAll:
	docker rmi ${IMAGE}:base
	docker rmi ${IMAGE}:build
