package main

import (
	"bitbucket.org/keekee-proj/service-observer/pkg/plugin"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	paramUrl     = "url"
	paramMethod  = "method"
	paramHeaders = "headers"
)

type sender struct{}

func (T *sender) Exec(entityType string, entityId string, event string, subject string,
	data []byte, params map[string]interface{}) ([]byte, error) {
	if _, ok := params[paramUrl]; !ok {
		return data, fmt.Errorf("url not set")
	}
	resource := params[paramUrl].(string)

	if resource == "" {
		return data, fmt.Errorf("url is empty")
	}

	method := "POST"
	m, ok := params[paramMethod]
	if ok {
		method = m.(string)
	}

	cli := http.Client{}
	postData := bytes.NewReader(data)
	req, err := http.NewRequest(
		method,
		makeUrl(resource, entityId, entityType, event),
		postData,
	)

	if err != nil {
		return data, err
	}

	if headers, ok := params[paramHeaders]; ok {
		headersList, ok := headers.(map[string]string)
		if ok {
			for key, val := range headersList {
				req.Header.Add(key, val)
			}
		}
	}

	resp, err := cli.Do(req)
	if err == nil && (resp.StatusCode == 200 || resp.StatusCode == 201) {
		return data, nil
	}

	if err == nil {
		respBody, _ := ioutil.ReadAll(resp.Body)
		err = fmt.Errorf("%s (%d): [%s]", resp.Status, resp.StatusCode, string(respBody))
	}
	return data, err
}

func (T *sender) Name() string {
	return "sender_http"
}
func (T *sender) Description() string {
	return "Send response by http"
}

func (T *sender) Scheme() map[string]*plugin.SchemeAttribute {
	return map[string]*plugin.SchemeAttribute{
		paramUrl: {
			Type:        "string",
			Require:     true,
			Default:     nil,
			Description: `url of resource`,
		},
		paramMethod: {
			Type:        "string",
			Require:     false,
			Default:     "POST",
			Description: `request method`,
		},
		paramHeaders: {
			Type:        "map",
			Require:     false,
			Default:     nil,
			Description: `request headers (key: value)`,
		},
	}
}
func (T *sender) PluginVersion() int {
	return 0
}
func (T *sender) MinAppVersion() int {
	return 0
}
func (T *sender) MaxAppVersion() int {
	return 0
}
func (T *sender) MinPluginApiVersion() int {
	return 0
}

func (T *sender) MaxPluginApiVersion() int {
	return 0
}
func makeUrl(resource string, entityId, entityType, event string) string {
	url := strings.Replace(resource, "<id>", entityId, -1)
	url = strings.Replace(url, "<entity>", entityType, -1)
	url = strings.Replace(url, "<event>", event, -1)
	return url
}

var Plugin sender
