### Example plugin for [service-observer](https://bitbucket.org/keekee-proj/service-observer/src/master/)

For building plugin call
```bash
make build
```

and plugin file will be saved to `./bin/sender_http.so`

And you can rebuild plugin by the next command:
```bash
make rebuild
```